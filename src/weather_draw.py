#!/usr/bin/python
# -*- coding:utf-8 -*-

import sys
import os

picdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'pic')
libdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'lib')
if os.path.exists(libdir):
    sys.path.append(libdir)

import logging
from time import time
import datetime
import traceback
from PIL import Image, ImageDraw, ImageFont
import traceback

logging.basicConfig(level=logging.DEBUG)

try:
    logging.info("Start to draw.")

    font24 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 24)
    font18 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 18)

    canvas = Image.new('1', (640,384), "white")

    draw = ImageDraw.Draw(canvas)
    now = datetime.datetime.now()
    current_time = now.strftime("%H:%M")
    draw.text((5,5), "Updated at {}".format(current_time), 'black', font24)    
    
    canvas.show()

except IOError as e:
    logging.info(e)

except KeyboardInterrupt:
    logging.info("ctrl+c:")
    exit()
